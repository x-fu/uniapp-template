/*
 * @Author: zy
 * @Date: 2022-07-24 22:51:41
 * @LastEditTime: 2022-07-26 15:44:49
 * @Description: 全局过滤器
 * 时间格式使用https://www.uviewui.com/js/time.html
 */

// 性别
export function getSexName(sex) {
  let str = "";
  if (sex == 0 || sex == "0") {
    str = "未知";
  } else if (sex == 1 || sex == "1") {
    str = "男";
  } else if (sex == 2 || sex == "2") {
    str = "女";
  }
  return str;
}

/*
 * 参数说明：
 * number：要格式化的数字
 * decimals：保留几位小数
 * dec_point：小数点符号
 * thousands_sep：千分位符号
 * */
export function formatAmount(number, sign = "--") {
  let decimals = 2,
    dec_point = ".",
    thousands_sep = ",";
  if (number === null || typeof number === "undefined") {
    return sign;
  }
  number = (number + "").replace(/[^0-9+-Ee.]/g, "");
  let n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 2 : Math.abs(decimals),
    sep = typeof thousands_sep === "undefined" ? "," : thousands_sep,
    dec = typeof dec_point === "undefined" ? "." : dec_point,
    s = "",
    toFixedFix = function(n, prec) {
      let k = Math.pow(10, prec);
      return "" + Math.ceil(n * k) / k;
    };

  s = (prec ? toFixedFix(n, prec) : "" + Math.round(n)).split(".");
  let re = /(-?\d+)(\d{3})/;
  while (re.test(s[0])) {
    s[0] = s[0].replace(re, "$1" + sep + "$2");
  }

  if ((s[1] || "").length < prec) {
    s[1] = s[1] || "";
    s[1] += new Array(prec - s[1].length + 1).join("0");
  }
  return s.join(dec);
}

// 手机号脱敏
export const formatPhone = (str, sign = "") => {
  if (!str) return sign;
  let pat = /(\d{3})\d*(\d{4})/;
  return str.replace(pat, "$1****$2");
};

// 银行卡脱敏
export const formatBankAccount = (str, sign = "") => {
  if (!str) return sign;
  let pat = /(\d{4})\d*(\d{3})/;
  return str.replace(pat, "$1********$2");
};

// 姓名脱敏
export const formatName = items => {
  if (!items) {
    return "";
  }
  if (items.length === 3) {
    return "*" + items.substr(-2);
  }
  if (items.length > 3) {
    let result = "";
    for (var i = 0; i < items.length - 2; i++) {
      result = result + "*";
    }
    return items.substr(0, 1) + result + items.substr(-1);
  }
  return "*" + items.substr(-1);
};

// 从html获取文字
export function trimHtml(str) {
  str = str.replace(/(\n)/g, "");
  str = str.replace(/(\t)/g, "");
  str = str.replace(/(\r)/g, "");
  str = str.replace(/<\/?[^>]*>/g, "");
  str = str.replace(/\s*/g, "");
  str = str.replace(/<[^>]*>/g, "");
  return str;
}


