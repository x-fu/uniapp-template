/*
 * @Author: zy
 * @Date: 2022-06-07 19:14:17
 * @LastEditTime: 2022-07-26 21:42:50
 * @Description:会员相关接口
 */
import { fetch } from "../fetch";
import $store from "@/store";

export default {
  // 个人信息
  getMemberInfo(loading = false, error = false, login = true) {
    return new Promise((resolve, reject) => {
      fetch({
        api: "/api/xf/wxuser/myInfo",
        loading,
        auth: true,
        error,
        login
      }).then(
        res => {
          $store.dispatch("setUserSaveInfo", res);
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  },
  updateMemberInfo(params, loading = true) {
    return fetch({
      api: "/api/xf/wxuser/updateMyInfo",
      params,
      loading,
      auth: true
    });
  },
  deleteMember(loading = true) {
    return fetch({
      api: "/api/xf/wxuser/deleteMember",
      loading,
      auth: true
    });
  },
  /**
   * 服务反馈
   */
  addFeedback(params) {
    return fetch({
      api: "/api/xf/wxuser/addFeedback",
      params: params
    });
  },
  // 微信用户列表
  getMemberList(params, loading = false) {
    return fetch({
      api: "/api/xf/wxuser/getMemberList",
      loading: loading,
      auth: false,
      params
    });
  },
  // 微信用户信息
  getWxMemberInfo(id, loading = true) {
    return fetch({
      api: "/api/xf/wxuser/getWxMemberInfo",
      loading: loading,
      auth: false,
      params: { id: id }
    });
  }
};
