/*
 * @Author: zy
 * @Date: 2022-06-07 19:14:17
 * @LastEditTime: 2022-07-25 22:21:46
 * @Description:Api主文件
 */
import commonApis from "./apis/common";
import memberApis from "./apis/member";
import wxApis from "./apis/wx";

export default {
  ...commonApis,
  ...memberApis,
  ...wxApis
};
