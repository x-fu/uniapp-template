package cn.jbolt.api.common.controller;

import cn.jbolt.base.JBoltProSystemLogTargetType;
import cn.jbolt.core.api.*;
import cn.jbolt.core.api.httpmethod.JBoltHttpPost;
import cn.jbolt.core.crossorigin.CrossOrigin;
import cn.jbolt.core.model.Application;
import cn.jbolt.core.model.SystemLog;
import cn.jbolt.core.para.JBoltPara;
import com.jfinal.aop.Inject;
import com.jfinal.core.Path;

/**
 * 微信小程序
 *
 * @ClassName: XfWxUserApiController
 * @author: xiaofu QQ：593391262
 * @date: 2022年7月25日
 */
@CrossOrigin
@Path("/api/xf/wxuser")
public class XfWxUserApiController extends JBoltApiBaseController {

    @Inject
    private XfUserApiService userApiService;

    /**
     * wxLogin
     * 只允许POST提交
     * 签发JWT
     */
    @JBoltApplyJWT
    @JBoltHttpPost
    public void login(String code) {
        renderJBoltApiRet(userApiService.login(code));
    }


    /**
     * wxLogin
     * 只允许POST提交
     * 签发JWT
     */
    @JBoltApplyJWT
    @JBoltHttpPost
    public void registerByPhone(JBoltPara param) {
        renderJBoltApiRet(userApiService.registerByPhone(param));
    }

    public void updateWechatPhone(JBoltPara param) {
        renderJBoltApiRet(userApiService.updateWechatPhone(param));
    }

    /**
     * 获取自己的用户信息
     */
    //@ActionKey("/xcx/api/user/me")
    public void myInfo() {
        renderJBoltApiRet(userApiService.getMyWechatUserInfo());
    }

    /**
     * 更新信息
     *
     * @param param
     */
    public void updateMyInfo(JBoltPara param) {
        renderJBoltApiRet(userApiService.updateUserInfo(param));
    }

    public void deleteMember(){
        renderJBoltApiRet(userApiService.deleteMember());
    }

    @OpenAPI
    public void getMemberList(JBoltPara param){
        renderJBoltApiRet(userApiService.getMemberList(param));
    }

    @OpenAPI
    public void getWxMemberInfo(JBoltPara param){
        renderJBoltApiRet(userApiService.getMemberInfo(param.getLong("id")));
    }

    public void addFeedback(JBoltPara param){
        String content = param.getString("content");
        String imgs = param.getString("imgs");
        Application application = JBoltApiKit.getApplication();
        JBoltApiUser apiUser = JBoltApiKit.getApiUser();
        String userName = apiUser.getUserName();
        SystemLog log=new SystemLog();
        String title="新增吐槽";
        if(isOk(content)){
            title+=":"+content;
        }if(isOk(imgs)){
            title+=","+imgs;
        }
        log.setTitle(title);
        log.setType(1);
        log.setUserName(userName);
        log.setTargetType(JBoltProSystemLogTargetType.HIPRINT_TPL.getValue());
        log.save();
        renderJBoltApiRet(JBoltApiRet.API_SUCCESS);
    }
}
