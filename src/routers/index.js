/*
 * @Author: zy
 * @Date: 2022-06-12 10:53:28
 * @LastEditTime: 2022-07-26 22:25:33
 * @Description:路由配置
 */
import { RouterMount, createRouter } from "uni-simple-router";
import indexRouter from "./modules/index";
import userRouter from "./modules/user";
import authRouter from "./modules/auth";
import jboltRouter from "./modules/jbolt";
import { devMode } from "@/config/index";
const router = createRouter({
  platform: process.env.VUE_APP_PLATFORM,
  routes: [...indexRouter, ...userRouter, ...authRouter, ...jboltRouter]
});
//全局路由前置守卫
router.beforeEach((to, from, next) => {
  if (devMode) {
    console.log("跳转前");
  }
  next();
});
// 全局路由后置守卫
router.afterEach((to, from) => {
  if (devMode) {
    console.log(to);
    console.log("跳转结束");
    //#ifdef H5
    setTimeout(() => {
      console.log("当前路径", location.href);
    }, 100);
    //#endif
  }
});

export { router, RouterMount };
