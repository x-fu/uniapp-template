/*
 * @Author: zy
 * @Date: 2022-06-12 10:53:28
 * @LastEditTime: 2022-07-26 22:24:55
 * @Description:jbolt实例页面
 */
export default [
  {
    path: "/pages/jbolt/index/index"
  },
  {
    path: "/pages/jbolt/user/ucenter/index"
  },
  {
    path: "/pages/jbolt/user/login/index"
  },
  {
    path: "/pages/jbolt/user/register/index"
  },
  {
    path: "/pages/jbolt/user/bind/index"
  },
  {
    path: "/pages/jbolt/user/syslogin/index"
  },
  {
    path: "/pages/jbolt/pay/index"
  }
];
