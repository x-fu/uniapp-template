package cn.jbolt.api.common.controller;

import cn.jbolt.admin.wechat.config.WechatConfigKey;
import cn.jbolt.core.cache.JBoltApplicationCache;
import cn.jbolt.core.cache.JBoltWechatConfigCache;
import cn.jbolt.core.controller.base.JBoltBaseController;
import cn.jbolt.core.crossorigin.CrossOrigin;
import cn.jbolt.core.model.Application;
import cn.jbolt.core.model.WechatConfig;
import cn.jbolt.core.para.JBoltPara;
import com.jfinal.aop.Clear;
import com.jfinal.core.Path;
import com.jfinal.kit.JsonKit;

/**
 * 公开访问的 后端 小程序API
 *
 * @ClassName: XfOpenApiController
 * @author: xiaofu QQ：593391262
 * @date: 2022年7月26日
 */
@CrossOrigin
@Path("/api/xf/open")
public class XfOpenApiController extends JBoltBaseController {


    @Clear
    public void wxaqrcode(JBoltPara param) {
        String jbId = param.getString("jbId");
        String page = param.getString("page");
        String scene = param.getString("scene");
        Application appInfo = JBoltApplicationCache.me.getApiCallApplication(jbId);
        if (appInfo != null) {
            Long mpId = appInfo.getLinkTargetId();
            //小程序Id
            String xcxAppId = JBoltWechatConfigCache.me.getConfigValue(mpId, WechatConfig.TYPE_BASE, WechatConfigKey.APP_ID);
            String xcxSecret = JBoltWechatConfigCache.me.getConfigValue(mpId, WechatConfig.TYPE_BASE, WechatConfigKey.APP_SECRET);
            String accessToken = WxXcxUtil.getAccessToken(xcxAppId, xcxSecret);

            byte[] wxaQrcode = WxXcxUtil.getActivityQrCodeByAccessToken(accessToken, scene, page);
            if (wxaQrcode != null && wxaQrcode.length > 0) {
                renderByteToImage(wxaQrcode);
            } else {
                redirect("https://imgsxf.xiaofu.fun/upload/app/info/20220712/8b187beb9372448ab233d4794fd62739.jpg");
            }
        } else {
            redirect("https://pet.xiaofu.fun/assets/xf/img/logo6.png");
        }
    }
}
