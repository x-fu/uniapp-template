const MSG = {
    success: '成功',
    fail: '失败',
    error: '发生异常',
    neterror: '网络异常',
    loading: '加载中...',
    loadfail: '加载失败',
    byCode: {
        //这里按照code 定义对应的errorMsg
        4000: '系统异常',
        4004: '尚未登录'
    }
};
module.exports = {
    MSG: MSG
};
