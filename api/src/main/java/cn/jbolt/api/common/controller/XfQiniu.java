package cn.jbolt.api.common.controller;

import cn.jbolt.core.util.JBoltQiniuUtil;
import com.qiniu.util.Auth;
import com.qiniu.util.StringMap;

public class XfQiniu extends JBoltQiniuUtil {

    /**
     * 在Jbolt扩展文件key方法
     * @param accessKey
     * @param secretKey
     * @param bucket
     * @param key
     * @return
     */
    public static String genTokenByKey(String accessKey, String secretKey, String bucket,String key) {
        Auth auth = Auth.create(accessKey, secretKey);
        return auth.uploadToken(bucket, key, 3600L, (StringMap)null);
    }
}
