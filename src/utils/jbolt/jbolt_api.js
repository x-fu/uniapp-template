/**
 * JBolt极速开发平台 API配置列表
 */
const JBOLT_APIS = {
    //微信模块 内置API
    wechat: {
        login: {
            url: '/api/wxa/login',
            withJwt: false
        },
        //wxlogin
        decryptUserInfo: '/api/wxa/decryptUserInfo',
        //解密更新微信小程序用户信息
        decryptPhoneNumber: '/api/wxa/decryptPhoneNumber',
        //解密更新微信小程序手机号
        bindOtherUser: '/api/wxa/bindOtherUser',
        //绑定其他表的用户属于 例如学生 教师 系统用户
        unbindOtherUser: '/api/wxa/unbindOtherUser',
        //解绑
        bindUserType: {
            SYSTEM_USER: 1
        },
        //可绑定用户类型定义
        loginSystemUser: '/api/wxa/loginSystemUser',
        refreshJwt: {
            url: '/api/jwt/refresh',
            withRefreshJwt: true
        },
        //refreshJwt
        user: {
            me: '/api/wxa/user/me' //获取本人信息
        }
    },
    //测试调试模块
    test: {
        login: {
            url: '/api/test/login',
            withJwt: false
        },
        go: '/api/test/go',
        postModel: '/api/test/postModel',
        uploadImage: '/api/test/uploadImage',
        unifiedorder: '/api/test/unifiedorder' //统一下单 demo
    },
    //用户
    user: {},
    //电商
    mall: {}
};
module.exports = {
    JBOLT_APIS: JBOLT_APIS
};
