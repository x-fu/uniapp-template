/*
 * @Author: zy
 * @Date: 2022-06-07 19:14:18
 * @LastEditTime: 2022-07-25 20:49:24
 * @Description: Vuex Store
 */
import Vue from 'vue'
import Vuex from 'vuex'
import token from './modules/token'
import session from './modules/session'
import common from './modules/common'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    token,
    session,
    common
  }
});

export default store
