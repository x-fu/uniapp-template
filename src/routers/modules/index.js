/*
 * @Author: zy
 * @Date: 2022-06-12 10:53:28
 * @LastEditTime: 2022-07-25 15:56:12
 * @Description: 首页相关路由
 */
export default [
  {
    path: "/pages/index/index",
    aliasPath: "/",
    name: "index"
  },
  {
    path: "/pages/web/link"
  },
  {
    path: "/pages/index/user"
  }
];
