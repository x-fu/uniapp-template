# 小弗-uniapp-template

**一个可以快速上手开发企业级uniapp的基础包，vue2 cli构建。**

1. 集成uview-ui，colorui，l-painter，mescroll, uni-simple-router

2. 内含通用css样式类，大量样式无需手写 直接复用

3. 更优雅方便的services api模块，集中管理，日志输出 上传文件等

4. vuex全局session token common模块。实现无感登录，自动刷新token

5. utils工具类，consts常量，filters过滤器，platform平台

6. mixins全局通用方法

7. 支持h5调试模式：开发小程序时，经常微信小程序ide响应延迟，影响开发速度，可以复制微信小程序token ，在chrome浏览器开发

8. 后端api接口已开源，源码在项目内

9. 项目目录结构清晰，新项目推荐直接使用，内无垃圾

10. demo内置功能：登录 ，注册，生成海报，上传文件，上传七牛文件，支付测试，分页下拉刷新，用户信息，修改资料，修改手机号，删除注销登录，全局登录状态，缓存等。扫码即可体验

    ----

    

本是一名后端开发人员，工作中也积累了几年的前端经验，现在开源希望对大家有一些帮助！一起交流！

![扫码体验](https://gitee.com/x-fu/uniapp-template/raw/master/xiaofu-open.jpg)

----



### 1）services api

+ 封装uni.request请求参数
+ ![日志](https://gitee.com/x-fu/uniapp-template/raw/master/public/api-log.jpg)

+ ```
  import $api from './services/index'
  Vue.prototype.$api=$api
  ```

+ ```
  使用api
  1.简化
  this.memberInfo = await this.$api.getWxMemberInfo(this.id)
  2.promise方式
  this.$api.getMemberList(params).then(res => {
   }, err => {
      this.newToast('数据加载失败')
  })
  ```

+ 上传文件支持自动获取token，上传至七牛服务器，返回文件地址

+ ```
  const file_url = await this.$api.autoUploadQiniu(fileExt, 'uni/xcx/upload/' + createPicKey(),bucketSn, file);
  ```

### 2） vuex store

+ common 通用信息

+ ```
  从服务器获取应用信息 getDefaultAppInfo
  this.$store.dispatch('getDefaultAppInfo')
  ```

+ session 微信小程序sessionKey封装

+ ```
  this.$store.dispatch('getSessionKey')
  ```

+ token 无感登录，自动保存服务器密钥，用户信息等

  ```
  this.$store.dispatch('toLogin')
  ```

### 3）styles

+ 样式全局引用colorui http://demo.color-ui.com/h5.html#/
+ style.css 添加常用*公共样式*，text-color，fontsize，flex布局等
+ 后端人员福音，直接复制就行了，不用手写！

### 4）components

+ 集成uview2.0 https://www.uviewui.com/ 需要的组件直接复制
+ l-painter https://gitee.com/liangei/lime-painter

+ mescroll http://www.mescroll.com/
+ xf-poster 快速生成海报
+ xf-image-upload 快速文件上传

### 5） routers

+ 集成uni-simple-router https://hhyang.cn/src/router/start/quickstart.html

+ 路由卫士

### 6）jbolt

+ 后台接口使用java jbolt开发
+ https://uniapp.xiaofu.fun/ 演示地址
+ xf模块api开源

![jbolt介绍](https://gitee.com/x-fu/uniapp-template/raw/master/public/jbolt-api.jpg)



----



作者微信：xiaofu-fun 有问题给5星后加作者微信 可得到及时的解答

本是一名后端开发人员，工作中也积累了几年的前端经验，现在开源希望对大家有一些帮助！一起交流！

### 页面展示

![页面](https://gitee.com/x-fu/uniapp-template/raw/master/xiaofu-page.jpg)

![首页](https://gitee.com/x-fu/uniapp-template/raw/master/public/page-index.jpg)