package cn.jbolt.api.common.controller;

import cn.jbolt._admin.user.UserService;
import cn.jbolt.common.model.Qiniu;
import cn.jbolt.common.model.QiniuBucket;
import cn.jbolt.common.util.CACHE;
import cn.jbolt.core.api.JBoltApiBaseController;
import cn.jbolt.core.api.JBoltApiKit;
import cn.jbolt.core.api.JBoltApiRet;
import cn.jbolt.core.api.OpenAPI;
import cn.jbolt.core.crossorigin.CrossOrigin;
import cn.jbolt.core.model.Application;
import cn.jbolt.core.model.User;
import cn.jbolt.core.para.JBoltPara;
import cn.jbolt.core.util.JBoltArrayUtil;
import com.jfinal.aop.Inject;
import com.jfinal.core.Path;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Page;

@CrossOrigin
@Path("/api/xf/common")
public class XfCommonApiController extends JBoltApiBaseController {

    @Inject
    private UserService userService;

    @OpenAPI
    public void appInfo() {
        Application application = JBoltApiKit.getApplication();
        renderJBoltApiRet(JBoltApiRet.API_SUCCESS_WITH_DATA(application));
    }

    @OpenAPI
    public void getAdminUserList(JBoltPara param) {
        Integer pageNumber = param.getInteger("pageNumber");
        Integer pageSize = param.getInteger("pageSize");
        if (notOk(pageNumber)) {
            pageNumber = 1;
        }
        if (notOk(pageSize)) {
            pageSize = 1;
        }
        Page<User> data = userService.paginate(pageNumber, pageSize);
        renderJBoltApiRet(JBoltApiRet.API_SUCCESS_WITH_DATA(data));
    }

    @OpenAPI
    public void getQiniuToken(JBoltPara param) {
        String fileKey = param.getString("fileKey");
        String extension = param.getString("extension");
        String bucketSn = param.getString("bucketSn");
        if (isOk(fileKey) && isOk(extension)) {
            fileKey = "demo/" + fileKey + "." + extension;
        } else {
            renderJBoltApiFail("参数错误");
            return;
        }
        if (notOk(bucketSn)) {
            renderJBoltApiFail("bucketSn错误");
            return;
        }
        Ret ret = genUploadParas(bucketSn, fileKey);
        if (ret.isOk()) {
            Kv result = ret.getAs("data");
            renderJBoltApiSuccessWithData(result);
        } else {
            renderJBoltApiFail(ret.getStr("msg"));
        }

    }

    /**
     * 生成指定bucket的token & region
     *
     * @param bucketSn
     * @return
     */
    private Ret genUploadParas(String bucketSn, String key) {
        Qiniu qiniu = null;
        QiniuBucket qiniuBucket = null;
        //如果没传或者传default 就获取默认default
        if (notOk(bucketSn) || bucketSn.trim().equals("default")) {
            //获取默认七牛账号
            qiniu = CACHE.me.getQiniuDefault();
            if (qiniu == null) {
                return Ret.fail("未指定bucket的sn将使用默认七牛账号，但系统尚未设置默认七牛账号");
            }
            qiniuBucket = CACHE.me.getQiniuDefaultBucket(qiniu.getId());
            if (qiniuBucket == null) {
                return Ret.fail("未指定bucket的sn将使用默认七牛账号，但默认七牛账号里尚未设置默认Bucket");
            }
        } else {
            qiniuBucket = CACHE.me.getQiniuBucketBySn(bucketSn);
            if (qiniuBucket == null) {
                return Ret.fail("指定的bucketSn无效");
            }
            qiniu = CACHE.me.getQiniu(qiniuBucket.getQiniuId());
            if (qiniu == null) {
                return Ret.fail("未找到指定bucket所属七牛账号信息");
            }
        }
        String domain = qiniuBucket.getDomainUrl();
        if (notOk(domain)) {
            return Ret.fail("bucket[" + qiniuBucket.getName() + ":+" + qiniuBucket.getSn() + "]未设置绑定域名");
        }
        if (domain.indexOf(",") != -1) {
            String[] urlArr = JBoltArrayUtil.from(domain, ",");
            if (urlArr == null || urlArr.length == 0) {
                return Ret.fail("bucket[" + qiniuBucket.getName() + ":+" + qiniuBucket.getSn() + "]设置绑定域名格式不正确");
            }
            domain = urlArr[0];
        }
        Kv result = Kv.by("token", XfQiniu.genTokenByKey(qiniu.getAk(), qiniu.getSk(), qiniuBucket.getName(), key));
        result.set("region", qiniuBucket.getRegion());
        result.set("domain", domain);
        result.set("key", key);
        result.set("file_url", domain + "/" + key);
        return Ret.ok().set("data", result);
    }
}
