/*
 * @Author: zy
 * @Date: 2022-06-09 20:25:28
 * @LastEditTime: 2022-07-26 21:19:45
 * @Description: 
 */
import { fetch, METHOD } from "../fetch";
export default {
  // 微信小程序登录
  wxLogin(code) {
    return new Promise((resolve, reject) => {
      fetch({
        api: "/api/xf/wxuser/login",
        params: { code },
        auth: false,
        error: true
      }).then(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  },
  // 使用手机号进行小程序注册
  registerByPhone(params) {
    return fetch({
      api: "/api/xf/wxuser/registerByPhone",
      params: params,
      loading: false,
      auth: false,
      error: true
    });
  },
  // 微信小程序更新手机号
  wxUpdatePhone(params) {
    return fetch({
      api: "/api/xf/wxuser/updateWechatPhone",
      params: params,
      auth: true
    });
  },
  // 微信小程序刷新token
  wxRefreshToken() {
    return fetch({
      api: "/api/jwt/refresh",
      auth: true,
      withRefreshJwt: true,
      method: METHOD.GET
    });
  }
};
