/*
 * @Author: zy
 * @Date: 2022-06-07 19:14:18
 * @LastEditTime: 2022-07-26 16:25:32
 * @Description: 小程序sessionKey
 */
import { localStorageKey } from "@/config";
import wxApis from "@/services/apis/wx";

export default {
  state: {},
  actions: {
    saveSessionKey({ state }, info) {
      if (info.sessionKey) {
        let nowDateTime = new Date().getTime();
        //默认6个小时过期
        let expireTime = (info.expires_in || 60 * 60 * 6) * 1000;
        console.log(
          "SessionKey失效时间:",
          uni.$u.timeFormat(
            new Date(nowDateTime + expireTime),
            "yyyy年mm月dd日 hh时MM分ss秒"
          )
        );
        uni.setStorageSync(localStorageKey.sessionKey, info.sessionKey);
        uni.setStorageSync(
          localStorageKey.sessionKeyExpireTime,
          nowDateTime + expireTime
        );
      }
      return true;
    },
    getSessionKey({ dispatch }) {
      return new Promise((resolve, reject) => {
        let sessionKey = uni.getStorageSync(localStorageKey.sessionKey);
        let expireTime = uni.getStorageSync(
          localStorageKey.sessionKeyExpireTime
        );
        let nowDateTime = new Date().getTime();
        console.log("sessionKey:", sessionKey);
        console.log("sessionKey过期时间:", new Date(expireTime));
        if (sessionKey && nowDateTime < expireTime) {
          uni.checkSession({
            success(res) {
              console.log("sessionKey有效返回");
              resolve({
                sessionKey
              });
            },
            fail(checkErr) {
              console.log("checkSession检测无效:", checkErr);
              dispatch("wxLogin").then(
                res => {
                  resolve(res);
                },
                err => {
                  reject(err);
                }
              );
            }
          });
        } else {
          console.log("无sessionKey或过期重新获取");
          dispatch("wxLogin").then(
            res => {
              resolve(res);
            },
            err => {
              reject(err);
            }
          );
        }
      });
    },
    wxLogin({ dispatch }) {
      return new Promise((resolve, reject) => {
        uni.login({
          provider: "weixin",
          success(loginRes) {
            // 调用接口进行登录
            wxApis.wxLogin(loginRes.code).then(
              res => {
                //console.log("session wxLogin:", res);
                dispatch("saveSessionKey", res);
                resolve(res);
              },
              err => {
                //console.log("session wxLogin err:", err);
                if (err.code === 4080) {
                  dispatch("saveSessionKey", err.data);
                }
                reject(err);
              }
            );
          },
          fail(err) {
            console.log("调用微信登录失败:", err);
            reject(err);
          }
        });
      });
    },
    removeSessionKey() {
      uni.removeStorageSync(localStorageKey.sessionKey);
      uni.removeStorageSync(localStorageKey.sessionKeyExpireTime);
    }
  },
  getters: {}
};
